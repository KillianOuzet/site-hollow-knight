# Tp Dev-Web, créer son propre site du début à la fin

## Explication

Durant ce tp nous devions produire un site avec un minimum de 2 pages en rapport avec une série ou un jeu que nous aimions. Sur celui-ci devait apparaître obligatoirement une page d'accueil ainsi qu'une page avec des personnages qui elle devait suivre la forme précisé dans la consigne.

## 🔗 Dépot gitlab du site

[![Gitlab](https://cdn.dribbble.com/users/191651/screenshots/4397812/click-me.gif)](https://gitlab.com/KillianOuzet/site-hollow-knight.git)

## Mon travail

Pour ma part j'ai produit un site avec un total de 5 pages sur le jeu Hollow knight. Sur chaque page j'ai tenté différentes propriétés CSS pour essayer de découvrir plus en profondeur ce language.  
Cela m'a énormément intéressé et j'ai donc passé beaucoup de temps sur mon site pour apprendre à faire plein de petit style différent.

### Ma page d'accueil

![screenshot de la page d'accueil](Screen-Accueil.png)

Voici une capture d'écran d'une partie de la page d'accueil où vous pouvez voir les éléments principaux de ma page. Tout d'abord mes 5 pages contiennent une forme principale identique, c'est-à-dire que tous comprennent une bannière avec un nav en header puis un main, un aside et un footer. À savoir que l'aside reste à droite à sa place quand l'on scroll vers le bas durant la navigation grâce à la déclaration "position: sticky;". De plus chaque lien de mon site envoient sur un nouvel onglet qui s'ouvre en même temps au lieu de recharger à la place du site.  
Sur la page d'accueil j'ai exposé en premier lieu un trailer du jeu vidéo en question et ensuite à l'aide de différents paragraphes je l'ai expliqué (Jouabilité,développement, réception etc...). Pour incruster la vidéo du trailer j'ai dù télécharger la vidéo en la convertissant en fichier mp4 puis j'ai utilisé la balise vidéo pour l'afficher sur la page sans erreurs.

### Les Personnages

![screenshot de la page d'accueil](Screen-Personnages.png)

Voici la deuxième page qui devait être obligatoirement faite. Ici j'ai présenté plusieurs personnages que vous rencontrez lors de l'aventure ainsi que le celui jouable(Le chevalier). Ces personnages sont dans le main de la page que j'ai choisi de laisser sans border, je trouvais que cela était plus propre et que ça faisait respirer la page. De plus, pour donner un style en plus j'ai justifié les textes du coté ou l'image de leur personnage était. Comme vous pouvez le voir sur la capture plus haut le texte en rapport avec le chevalier est aligné sur la gauche alors que le texte du personnage Hornet est aligné sur la droite.

### L'histoire

![screenshot de la page d'accueil](Screen-Histoire.png)

Ma 3ème page repose sur l'histoire d'Hollow knight, pour cela j'y ai expliqué le scénario du jeu en grande ligne. c'est sur cette page que j'ai testé le style le plus compliqué, vous pourrez y voir une bordure animée avec des couleurs en lien avec la bannière du header. Pour faire cela j'ai recherché à faire des bordures originales et différentes que de simples traits et c'est là que j'ai appris que nous pouvions faire des bordures animées en CSS. j'ai donc recherché un style simple d'une d'entre elles en essayant de la comprendre, et j'ai pu retirer les propriétés du codes inutiles et/ou nocives à la structure de mon site puis y ai changé les couleurs pour faire un lien avec le site. Ce code reste quand même complexe et va me demander plus temps pour comprendre comment reproduire cela en autonomie.

### La Map du jeu

![screenshot de la page d'accueil](Screen-Map.png) 

Cette avant-dernière page est la présentation de la carte du jeu, celle-ci étant très grande je n'y ai pas inclus toutes les zones mais seulement les toutes premières que l'on parcourt au début de la progression. Pour faire cela j'ai fait plusieurs boites pour chaque zone avec l'image à gauche et une petite présentation à droite. En mettant les images à gauche j'ai pu apercevoir qu'un bout de leurs coins sortait du cadre de la section j'ai donc utilisé pour la première fois dans ce site la propriété overflow avec la valeur Hidden. Celle-ci permet de ne pas afficher ce qui sort du container et donc ce coin qui sortait.

### La boutique

![screenshot de la page d'accueil](Screen-Boutique.png)

Pour cette dernière page nommée "Boutique" j'y ai simplement affiché les différentes plateformes où l'on peut acheter le jeu, celles-çi sont tirées du site officiel du jeu. Je n'ai utilisé qu'une seule section dans le main ou j'y ai placé plusieurs balises "a" avec des images inclus de façon à ce que chaque logo renvoie sur la page du site pour acheter Hollow Knight.

## Auteur

- [@KillianOuzet](https://gitlab.com/KillianOuzet)


## 🚀 About Me
Je suis étudiant en première année de BUT Info à l'iut d'Orléans


![Logo](https://lh5.googleusercontent.com/p/AF1QipNG6uIdBx1-r5BtKAhxKsEy0Qk-6ZsJSQuq_HC9=w660-h353-p-k-no)
